# News API Reader
An application that reads the top stories from any of NewsAPI.org's 70+ sources.

## Frameworks
* [AlamoFire](https://github.com/Alamofire/Alamofire)
* [Marshal](https://github.com/utahiosmac/Marshal)
* [IGListKit](https://github.com/Instagram/IGListKit)